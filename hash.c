#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define M 5
#define tamNome 50
#define tamMatricula 12

typedef enum {False = 0, True} Boolean;

typedef enum {sair = 0, cadastrar, buscar, remover, exibir} itemMenu;

typedef struct Aluno{
	
	char matricula[tamMatricula];
	char nome[tamNome];
	int idade;

	struct Aluno *proximo;

}stAluno;

void apertarenter(){
	char bufLixo[50];
	printf("\nAperte enter para continuar\n");
	fgets(bufLixo, 3, stdin);
}

void InicializaTabela(stAluno **tab){
	int i;
	for(i=0;i<M;i++)
		tab[i] = NULL;
}

Boolean ApenasNumeros(char *str, int tam){
	int i;
	for(i=0; i<tam-1; i++, str++)
		if(*str != '0' && *str != '1' && *str != '2' && *str != '3' && *str != '4' && *str != '5' && *str != '6' && *str != '7' && *str != '8' && *str != '9')
			return False;
	return True;
}

void CadastrarAluno(stAluno *aluno){
	char bufAux[50];

	printf("Digite o Nome\n");
	fgets(aluno->nome, tamNome, stdin);

	printf("Digite a matricula\n");
	do{
		fgets(aluno->matricula, tamMatricula, stdin);
	}while(!ApenasNumeros(aluno->matricula, strlen(aluno->matricula)));

	printf("Digite a idade\n");
	do{
		fgets(bufAux, 4, stdin);
	}while(!ApenasNumeros(bufAux, strlen(bufAux)));
	aluno->idade = atoi(bufAux);
}

int enderecoHash(char *strMatricula){
	
	int intMatricula;
	
	intMatricula = atoi(strMatricula);

	return intMatricula % M;

}

void imprimeAluno(stAluno *aluno, Boolean aprtrenter){
	printf("Aluno: %sMatricula: %sIdade: %d\n", aluno->nome, aluno->matricula, aluno->idade);
	if(aprtrenter)
		apertarenter();
}

void imprimeListaEncadeada(stAluno *lista){
	stAluno *p;
	p = lista;
	do{
		printf("xxx---xxx---xxx\n");
		imprimeAluno(p, False);
		p = p->proximo;
	}while(p != NULL);
	printf("xxx---xxx---xxx\n");
}

Boolean BuscaMatriculaListaEncadeada(stAluno *lista, char *strMatricula, Boolean imprimir){
	stAluno *p;
	p = lista;
	if(p==NULL)
		return False;
	do{
		if(!strcmp(strMatricula, p->matricula)){
			if(imprimir)
				imprimeAluno(p, True);
			return True;
		}
		p = p->proximo;
	}while(p != NULL);
	return False;
}
Boolean PerguntaSimNao(char *str){
	Boolean op;
	
	printf("%s\n", str);
	printf("1 Sim - 0 Nao\n");
	do{scanf("%d", &op);}while(op != True && op != False);

	return op;
}
Boolean RemoverMatriculaListaEncadeada(stAluno *lista, char *strMatricula, Boolean imprimir){
	stAluno *p, *anterior;
	p = lista;
	anterior = NULL;
	if(p==NULL){
		printf("Nao encontrado\n");
		return False;
	}
	do{
		if(!strcmp(strMatricula, p->matricula)){
			if(imprimir)
				imprimeAluno(p, False);

			if(PerguntaSimNao("Deseja excluir?")){
				if (anterior != NULL){
					if (p->proximo != NULL)
						anterior->proximo = p->proximo;
					else
						anterior->proximo = NULL;
					free(p);
				}
				else
					if(p->proximo != NULL){
						lista = p->proximo;
						free(p);
					}
					else {
						lista = NULL;
						free(p);
					}

				return True;
			}
			else{
				printf("Exclusao interrompida\n");
				return False;
			}
		}
		anterior = p;
		p = p->proximo;
	}while(p != NULL);

	printf("Nao encontrado\n");

	return False;
}
void BuscarAlunoPorMatricula(stAluno **tab){
	char bufMatricula[tamMatricula];
	int indice;

	printf("Digite a matricula\n");
	do{
		fgets(bufMatricula, tamMatricula, stdin);
	}while(!ApenasNumeros(bufMatricula, strlen(bufMatricula)));

	indice = enderecoHash(bufMatricula);

	if(!BuscaMatriculaListaEncadeada(tab[indice], bufMatricula, True)){
		printf("Matricula nao encontrada\n");
		apertarenter();
	}

}
void exibirTabHash(stAluno **tab){
	int i;
	for(i=0; i<M; i++){
		printf("--------------------------------------------\n");
		printf("--------> Indice %d da tabela hash <--------\n", i);
		if(tab[i] != NULL)
			imprimeListaEncadeada(tab[i]);
		else
			printf("Indice sem nenhum aluno.\n");

	}
	printf("--------------------------------------------\n");
	apertarenter();
}

void RemoverAlunoPorMatricula(stAluno **tab){
	char bufMatricula[tamMatricula];
	int indice;

	printf("Digite a matricula\n");
	do{
		fgets(bufMatricula, tamMatricula, stdin);
	}while(!ApenasNumeros(bufMatricula, strlen(bufMatricula)));

	indice = enderecoHash(bufMatricula);

	RemoverMatriculaListaEncadeada(tab[indice], bufMatricula, True);
	apertarenter();
}

void CadastroAluno(stAluno **tab){
	
	stAluno *aluno;
	int indice;

	aluno = malloc(sizeof(stAluno));
	aluno->proximo = NULL;

	CadastrarAluno(aluno);
	indice = enderecoHash(aluno->matricula);

	if(BuscaMatriculaListaEncadeada(tab[indice], aluno->matricula, False)){
		printf("Matricula existente.\n");
		apertarenter();
		return;
	}

	if (tab[indice] != NULL)
		aluno->proximo = tab[indice];

	tab[indice] = aluno;

}

itemMenu opcoesMenu(){

	int op;
	do{
		system("clear");
		printf("---------------------------------------------------------\n");
		printf("\t\t 1 - Cadastrar Aluno\n");
		printf("\t\t 2 - Buscar Aluno\n");
		printf("\t\t 3 - Remover Aluno\n");
		printf("\t\t 4 - Exibir TabelaHash\n");
		printf("\t\t 0 - Encerrar\n");
		printf("---------------------------------------------------------\n");
		scanf("%d", &op);
		if(!(op >= sair && op <= exibir)){
			printf("Opcao Invalida\n");
			apertarenter();
		}
	}while(!(op >= sair && op <= exibir));

	getchar();

	return op;
}

int main(int argc, char const *argv[])
{

	int i, cont=0;

	itemMenu op;

	stAluno aluno, aluno2;

	stAluno *TabHash[M];

	InicializaTabela(TabHash);

	do{
		op=opcoesMenu();
		switch (op){
			case cadastrar:
				CadastroAluno(TabHash);
				break;
			case buscar:
				BuscarAlunoPorMatricula(TabHash);
				break;
			case remover:
				RemoverAlunoPorMatricula(TabHash);
				break;
			case exibir:
				exibirTabHash(TabHash);
				break;
			case sair:
				printf("Programa Encerrado.\n");
				apertarenter();
				break;
		}
	}while(op!=sair);

	for(i=0;i<M;i++)
		if(TabHash[i] != NULL){
			printf("%d - %s\n", i, TabHash[i]->matricula);
			if(TabHash[i]->proximo != NULL)
				printf("conflitou\n");
			cont++;
		}

	printf("%d campos da tabela preenchido\n", cont);

	return 0;
 }





